import useGlobal from "@coralmango/use-swr-global"

interface HookReturnValue {
  selectedMenuKey: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setSelectedMenuKey: (value: any) => void
  collapsed: boolean
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setCollapsed: (value: any) => void
}

const useSider = (): HookReturnValue => {
  const [selectedMenuKey, setSelectedMenuKey] = useGlobal(
    "sider-selected-menu-key"
  )
  const [collapsed, setCollapsed] = useGlobal("siderCollapsed")

  return {
    selectedMenuKey,
    setSelectedMenuKey,
    collapsed,
    setCollapsed,
  }
}

export default useSider
