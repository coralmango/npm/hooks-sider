import { useEffect } from "react"
import useSider from "./use-sider"

const useSiderMenuKey = (selectedMenuKey: string): void => {
  const { setSelectedMenuKey } = useSider()
  useEffect(() => {
    setSelectedMenuKey(selectedMenuKey)
  }, [setSelectedMenuKey, selectedMenuKey])
}

export default useSiderMenuKey
